import {shallowMount} from '@vue/test-utils';
import BottleMarker from '@/components/BottleMarker.vue';

describe('BottleMarker.vue', () => {
    it('renders props.msg when passed', () => {
        const thickness: number = 20;
        const innerRadius: number = 40;
        const startAngleRad: number = 0;
        const endAngleRad: number = 30;
        const wrapper = shallowMount(BottleMarker, {
            propsData: {thickness, innerRadius, startAngleRad, endAngleRad},
        });
        expect(wrapper.html()).toMatch('200');
    });
});
