export interface Point {
    x: number;
    y: number;
}

export interface Handle {
    position: Point;
    adjacentHandleIndex: number;
}

export interface BottleNeckMeasure {
    topDiameter: number;
    bottomDiameter: number;
    height: number;
}
