import {Point} from '@/lib/global-types';


export interface ArcModel {
    start: Point;
    end: Point;
    pathSpec: string;
}


export function createArcModel(center: Point, radius: number, startAngle: number, endAngle: number): ArcModel {
    const angleBetween = normalizeAngleToMaxTwoPi(endAngle - startAngle);
    const start = computerPointOnCircle(center, radius, startAngle);
    const end = computerPointOnCircle(center, radius, endAngle);
    const invertFlag = angleBetween < Math.PI ? 1 : 0;
    const pathSpec = `M ${start.x} ${start.y} A ${radius} ${radius} ${invertFlag} ${invertFlag} 0  ${end.x} ${end.y}`;
    return {
        start, end, pathSpec,
    };
}

function normalizeAngleToMaxTwoPi(angle: number) {
    const mod = angle % (2 * Math.PI);
    if (mod < 0) {
        return mod + 2 * Math.PI;
    }
    return mod;
}

export function computerPointOnCircle(center: Point, radius: number, angleRad: number): Point {
    return {
        x: Math.cos(angleRad) * radius + center.x,
        y: Math.sin(angleRad) * radius + center.y,
    };
}
