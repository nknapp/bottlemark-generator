export interface ArcSpec {
    innerRadius: number;
    thickness: number;
    angle: number;

}

export function computeArcSpec(diameterTop: number, diameterBottom: number, height: number): ArcSpec {
    const diameterDiff = (diameterBottom - diameterTop);
    const diameterDiffOnOneSide = diameterDiff / 2;
    const thickness = Number(height);
    const angle = Math.PI * (2 - (diameterBottom - diameterTop) / height);
    const innerRadius = thickness * diameterTop / diameterDiff;
    return {
        innerRadius, thickness, angle,
    };
}
